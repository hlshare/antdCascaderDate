# antdCascaderDate
[![](https://badgen.net/github/stars/heerey525/antdCascaderDate?color=green)](https://github.com/heerey525/antdCascaderDate)
[![](https://badgen.net/github/forks/heerey525/antdCascaderDate?color=orange)](https://github.com/heerey525/antdCascaderDate)

对于antd Cascader 内中国省市区数据的补充，数据来源 2020月12月中华人民共和国县以上行政区划代码 http://www.mca.gov.cn/article/sj/xzqh/2020/20201201.html

202012及之前版本, 数据缺失补充说明：
> 缺失 三沙市下辖 西沙区、南沙区
> 缺失 海南省直辖县级市
> 缺失 新疆维吾尔自治区直辖县级市
> 缺失 湖北省直辖县级市
> 缺失 河南省直辖国家产城融合示范区

你需要的文件是：

Cascader级联数据：[cities.js](https://github.com/heerey525/antdCascaderDate/blob/master/src/cities.js)
省市二级联动数据：[city](https://github.com/heerey525/antdCascaderDate/tree/master/src/city)

用法参考：[antd级联选择](https://ant.design/components/cascader-cn/)
网页展示：[网页展示](https://heerey525.github.io/antdCascaderDate/dist/)

    当然你也可以直接下载 https://github.com/heerey525/antdCascaderDate.git
    然后安装依赖 npm install
    运行 npm start

最近发现很多人star这个项目，所以我新开一个项目将生成此数据的方法公开，https://github.com/heerey525/antdCascaderDateMethod 欢迎star
